from string import punctuation
import re
from spacy.lang.ru import Russian
from spacy_russian_tokenizer import RussianTokenizer, MERGE_PATTERNS
import pandas as pd
from collections import Counter, defaultdict
from torchtext.vocab import _default_unk_index


def preprocess(s):
    s_clean = s.lower().replace("\n", "").translate(str.maketrans(punctuation+"«"+"»"+"№"+"•", " "*36))
    s_cyrillic = re.sub(pattern="[a-z]|[A-Z]", repl="", string=s_clean)
    return s_cyrillic

def normalize(s, morph):
    lemmas = [morph.parse(w)[0].normal_form for w in preprocess(s).split()]
    return " ".join([lemma for lemma in lemmas])

def get_russian_tokenizer():
    nlp = Russian()
    russian_tokenizer = RussianTokenizer(nlp, MERGE_PATTERNS)
    nlp.add_pipe(russian_tokenizer, name='russian_tokenizer')
    return nlp

def tokenize(tokenizer):
    def callback(sentence):
        doc = tokenizer(sentence)
        return [token.text.lower() for token in doc if token.text not in punctuation and token.text != ' ']
    
    return callback

def read_corpus(paths, text_col):
    corpus = []
    for path in paths:
        corpus.append(pd.read_csv(path, usecols=[text_col], squeeze=True))
    corpus = pd.concat(corpus)
    return corpus.tolist()

def adjust_corpus_to_model(corpus, model, tokenizer):
    nlp = tokenizer()
    nlp.max_length = 4000000
    call_tokenizer = tokenize(nlp)
    corpus_string = ' '.join(corpus)
    corpus = call_tokenizer(corpus_string)
    corpus = [word for word in corpus if word in model]
    return corpus

def build_vocab(corpus):
    freqs = Counter(corpus)
    itos = ['<unk>', '<pad>']
    for word in set(corpus):
        itos.append(word)
    stoi = defaultdict(_default_unk_index)
    stoi.update({word: idx for (idx, word) in enumerate(itos)})
    return freqs, itos, stoi
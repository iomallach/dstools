from sklearn.model_selection import KFold, StratifiedKFold
from sklearn.utils.validation import check_is_fitted
from tqdm import tqdm
import numpy as np
from collections import defaultdict
import statistics as s


def get_cv_scores(clf, train, target, transformers, score_func, cv=5):
    aucroc_train = []
    aucroc_test = []
    
    if isinstance(cv, int):
        cv_inner = KFold(n_splits=cv, random_state=42, shuffle=True)
        pbar = tqdm(total=cv)
    else:
        cv_inner = cv
        pbar = tqdm(total=cv.get_n_splits())
    
    for train_index, test_index in cv_inner.split(train.index, target):
        clf_inner = clf
        inner_train = train.iloc[train_index].copy()
        inner_test = train.iloc[test_index].copy()
        if transformers is not None:
            for trf in transformers:
                try:
                    check_is_fitted(estimator=trf, attributes=['vocabulary_', 'components_'], all_or_any=any)
                except:
                    trf.fit(inner_train)
                try:
                    inner_train = trf.transform(inner_train).toarray()
                    inner_test = trf.transform(inner_test).toarray()
                except:
                    inner_train = trf.transform(inner_train)
                    inner_test = trf.transform(inner_test)

        clf_inner.fit(inner_train, target.iloc[train_index].copy())
        aucroc_train.append(score_func(y_pred=clf_inner.predict(inner_train), y_true=target.iloc[train_index].copy()))
        aucroc_test.append(score_func(y_pred= clf_inner.predict(inner_test), y_true=target.iloc[test_index].copy()))
        pbar.update(1)
    pbar.close()
    return aucroc_train, aucroc_test

def get_oof_score(n_splits, random_state, train, target, test, transformer, clf):
    kfold = KFold(n_splits=n_splits, random_state=random_state)
    oof_train = np.zeros((train.shape[0], 1))
    oof_test = np.zeros((test.shape[0], 1))
    oof_test_skf = []
    pbar = tqdm(total=5)
    for idx, (traindex, testdex) in enumerate(kfold.split(train, target)):
        x_tr = train.loc[traindex, :]
        x_te = train.loc[testdex, :]
        y_tr = target.loc[traindex]
        clf.fit(x_tr, y_tr)
        oof_train[testdex, :] = clf.predict(x_te).reshape(x_te.shape[0], -1)
        oof_test_skf.append(clf.predict(test).reshape(test.shape[0], -1))
        pbar.update(1)
    pbar.close
    # oof_test = oof_test_skf.mean(axis=0).T
    return oof_train, oof_test_skf

def get_nested_kfold_scores(X, target, clf, n_passes=10, nfolds=10):
	score_byfold = defaultdict(list)
	for n in range(n_passes):
		kfold = StratifiedKFold(n_splits=nfolds, random_state=n*10, shuffle=True)
		for idx, (traindex, testdex) in enumerate(kfold.split(X, target)):
			clf_inner = clf
			clf_inner.fit(X.loc[traindex, :].copy(), target.loc[traindex].copy())
			score_byfold['fold {}'.format(idx)].append(roc_auc_score(y_score=get_sklearn_pos_proba(X.loc[testdex, :], clf_inner), 
																	 y_true=target.loc[testdex]))
	return score_byfold

def get_sklearn_pos_proba(X, clf):
	return clf.predict_proba(X)[:, 1]

def get_nested_kfold_stats(d):
	for key in d.keys():
		yield (s.mean(d[key]), s.stdev(d[key), min(d[key]), max(d[key]))

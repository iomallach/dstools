import numpy as np


def categorical_accuracy(y_true, y_pred):
    assert len(y_true) == len(y_pred)
    return np.sum(y_true == y_pred) / len(y_true)
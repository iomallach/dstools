from matplotlib import pyplot as plt
from sklearn.metrics import roc_curve
from sklearn.base import ClassifierMixin
from lightgbm import Booster


def get_plot_instance(figsize=(10, 10), subplots=(1, 1)):
    f, ax = plt.subplots(*subplots, figsize=figsize)
    return f, ax

def set_figure_and_axes(f, ax):
    ax.grid(True)
    ax.set_facecolor('0.8')
    f.set_facecolor('0.7')
    return f, ax

def trim_axs(axes: 'numpy.ndarray[Axes]', n: int) -> 'numpy.flatiter[Axes]':
    a = axes.flat
    for ax in a[n:]:
        ax.remove()
    return a[:n]

def plot_multiple(source: pd.DataFrame, 
                  factor: str, 
                  target: str, 
                  kind: str, 
                  fsize: Tuple[int, int], 
                  max_cols: int = 4,
                  return_axes: bool = False) -> matplotlib.figure.Figure:
    uniques, nunique = source[factor].unique(), source[factor].nunique()
    nrows, ncols = int(np.floor(nunique / max_cols)), max_cols if max_cols <= nunique else nunique
    nrows = nrows if nrows * ncols >= nunique else nrows + 1 
    print(nrows, ncols)
    f, ax = plt.subplots(nrows, ncols, figsize=fsize)
    flat_axes = trim_axs(ax, nunique)
    for idx, level in enumerate(uniques):
        condition = f"'{level}'" if isinstance(level, str) else f"{level}"
        source.query(f"{factor} == {condition}")[target].plot(kind=kind, ax=flat_axes[idx])
    return (f, ax) if return_axes else f

# Sklearn and lightgbm plotter
def plot_roc_curve(x_train, x_test, y_train, y_test, clf, figsize=None, set_style=True, savename='blank'):
    assert x_train.shape[1] == x_test.shape[1], 'The number of features in train and test sets does not match. Make sure that the dataframes are correct'
    assert y_train.nunique() == 2 and y_test.nunique() == 2, 'The taget is not binary or the number of target levels in train and test sets does not match'
    assert isinstance(clf, ClassifierMixin) == 1 or isinstance(clf, Booster) == 1, 'CLF is not a valid classifier. Should be ClassifierMixin or Booster'
    assert isinstance(figsize, tuple) == 1 or isinstance(figsize, list) == 1 or figsize is None, 'FIGSIZE is not a valid object. Should be a tuple, a list or None'
    
    if figsize is None:
        f, ax = get_plot_instance()
    else:
        f, ax = get_plot_instance(figsize=figsize)
        
    if isinstance(clf, ClassifierMixin):
        y_score_train = clf.predict_proba(x_train)[:, 1]
        y_score_test = clf.predict_proba(x_test)[:, 1]
    else:
        y_score_train = clf.predict(x_train)
        y_score_test = clf.predict(x_test)
    
    fpr, tpr, thresholds = roc_curve(pos_label=1, y_true=y_train, y_score=y_score_train)
    ax.plot(fpr, tpr, label='Train')
    fpr, tpr, thresholds = roc_curve(pos_label=1, y_true=y_test, y_score=y_score_test)
    ax.plot(fpr, tpr, label='Test')
    ax.plot([0, 1], [0, 1], label='baseline')
    ax.legend(loc='best')
    ax.set_xlim([0, 1])
    ax.set_ylim([0, 1])
    if set_style:
        f, ax = set_figure_and_axes(f, ax)
    f.show()
    if savename != 'blank':
        f.savefig('./src/out/{}'.format(savename))


def plot_crossval_score(train_auc, test_auc, savename='blank', title='No title', figsize=(12, 8), savefig=False):
    f, ax = get_plot_instance(figsize=figsize)
    ax.plot(list(range(len(train_auc))), train_auc, label='Train i-th fold score')
    ax.plot(list(range(len(test_auc))), test_auc, label='Test i-th fold score')
    ax.grid(True)
    ax.set_facecolor('0.8')
    ax.plot(list(range(len(train_auc))), [np.array(train_auc).mean()]*len(train_auc), '--', label='Train mean reference')
    ax.plot(list(range(len(test_auc))), [np.array(test_auc).mean()]*len(test_auc), '-.', label='Test mean reference')
    ax.set_xlabel('Fold number')
    ax.set_ylabel('Cross validation scores')
    ax.set_title(title)
    ax.legend(loc='best')
    f.set_facecolor('0.7')
    f.show()
    if savefig:
        f.savefig(savename)

def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    f = plt.gcf()
    f.set_facecolor('0.7')
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

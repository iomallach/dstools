import pandas as pd
import pandas
import numpy as np


class TargetEncoder(object):
	def __init__(self, min_samples_leaf=1, smoothing=1, noise_level=0):
		self.min_samples_leaf = min_samples_leaf
		self.smoothing = smoothing
		self.noise_level = noise_level
	
	def check_series(self, series):
		assert len(series) == len(self.trn_series)
		assert self.trn_series_name == series.name
	
	def fit(self, trn_series, target):
		temp = pd.concat([trn_series, target], axis=1)
		self.trn_series_name = trn_series.name
		self.target_name = target.name
		# Compute target mean 
		self.averages = temp.groupby(by=trn_series.name)[target.name].agg(["mean", "count"])
		# Compute smoothing
		self.smoothing = 1 / (1 + np.exp(-(self.averages["count"] - self.min_samples_leaf) / self.smoothing))
		# Apply average function to all target data
		self.prior = target.mean()
		# The bigger the count the less full_avg is taken into account
		self.averages[target.name] = self.prior * (1 - self.smoothing) + self.averages["mean"] * self.smoothing
		self.averages.drop(["mean", "count"], axis=1, inplace=True)
		self.trn_series_index = trn_series.index
		return self
	
	def add_noise(self, series):
		return series * (1 + self.noise_level * np.random.randn(len(series)))
	
	def transform(self, series):
		ft_series = pd.merge(
        series.to_frame(series.name),
        self.averages.reset_index().rename(columns={'index': self.target_name, self.target_name: 'average'}),
        on=series.name,
        how='left')['average'].rename(self.trn_series_name + '_mean').fillna(self.prior)
		ft_series.index = series.index
		return self.add_noise(ft_series)
	
	def fit_transform(self, trn_series, target):
		return self.fit(trn_series, target).transform(trn_series)

class ProportionalResampler(object):
    '''
        Parameters:
            event_draw_fraction - what fraction of events to draw from X, a float from 0 to 1. Default: 1
            event_out_fraction - a fraction of events in a resampled DataFrame, a float from 0 to 1. Default: 0.5
            strata - a binary variable used to stratify resampling, an integer with values in [0, 1]. Default: 'target'
            random_state - a resample random state. Default: 42
            
            Example:
            X = pd.readcsv('./myfiles/mycsv.csv')
            sampler = ProportionalResampler(event_draw_fraction=1, event_out_fraction=0.05, strata='had_application', random_state=0)
            resampled_dataframe = sampler.fit_sample(X)
    '''
    def __init__(self, event_draw_fraction=1, event_out_fraction=0.5, strata='target', random_state=42):
        self.event_draw_fraction = event_draw_fraction
        self.event_out_fraction = event_out_fraction
        self.strata = strata
        self.random_state = random_state
        
    def fit(self, X):
        assert isinstance(X, pandas.core.frame.DataFrame), 'Input object is not a DataFrame instance'
        counts = X[self.strata].value_counts()
        self.event_draw_count =int( counts[1] * self.event_draw_fraction)
        self.non_event_draw_count = int(self.event_draw_count * (1 - self.event_out_fraction)/self.event_out_fraction)
        return self
    
    def sample(self, X):
        assert isinstance(X, pandas.core.frame.DataFrame), 'Input object is not a DataFrame instance'
        return pd.concat([X.query('{} == 0'.format(self.strata)).sample(n=self.non_event_draw_count, random_state=self.random_state),
                          X.query('{} == 1'.format(self.strata)).sample(n=self.event_draw_count, random_state=self.random_state)])
    
    def fit_sample(self, X):
        return self.fit(X).sample(X)

def add_date_features(X, date_vars):
    for date_var in [date_vars] if isinstance(date_vars, str) else date_vars:
        X['dayofweek_{}'.format(date_var)] = X[date_var].dt.dayofweek
        X['month_{}'.format(date_var)] = X[date_var].dt.month
        X['year_{}'.format(date_var)] = X[date_var].dt.year
        X['quarter_{}'.format(date_var)] = X[date_var].dt.qtr
    return X

class ImputeCaller(object):
    """A wrapper for multiple ImputeTransformers.
    
    A wrapper for multiple ImputeTransformers initializes with a list of ImputeTransformers, transforms all columns or subsets of columns, concatenates output Series and
    returns a pandas DataFrame containing all imputed variables with respect to their groups as passed into ImputeTransformer's '__init__'

    Parameters
    ----------
    transformers : list of ImputeTransformers, required

    Attributes
    ----------
    transformers : list of ImputeTransformers of shape [len(all_impute_variables)]
    """
    def __init__(self, transformers):
        self.transformers = transformers
    
    def fit_many(self, X):
        for i in range(len(self.transformers)):
            self.transformers[i].fit(X)
        return self
    
    def fit_subset(self, X, slices):
        for transformer in [x for x in self.transformers if x.impute_var in slices]:
            transformer.fit(X)
        return self

    def fit(self, X, slices=None, subset=False):
        if subset:
            return self.fit_subset(X, slices=slices)
        else:
            return self.fit_many(X)
    
    def transform(self, X):
        names = [x.impute_var for x in self.transformers if x.is_fitted == True]
        frame = pd.DataFrame(data=np.zeros((X.shape[0], len(names))), columns=names)
        index = X.index.copy()
        frame.index = index
        for transformer in [x for x in self.transformers if x.is_fitted == True]:
            frame.loc[:, transformer.impute_var] = transformer.transform(X)
        return frame
    
    def fit_transform(self, X, slices=None, subset=False):
        return self.fit(X=X, slices=slices, subset=subset).transform(X)


class ImputeTransformer(object):
    """A groupby-fashioned impute transformer.
    
    A groupby-fashioned impute transformer is a transformer that takes pandas group labels and pandas columns-to-impute label as well as an impute strategy (a python callable       in future releases, but a string as of now), imputes NANs in a desirable variable with a mean or median by given groups and returns an imputed Series or a DataFrame with
    the column-to-impute replaced by imputed column.

    Parameters
    ----------
    impute_with : string, required. Can be either 'mean' or 'median'
                  A function that returns an impute value within a group
    
    impute_var : string, required. A DataFrame passed into 'fit' must contain this column
                 A column-to-impute label

    impute_groups : list of strings or pandas Index instance , required. A DataFrame passed into 'fit' must contain columns under those labels
                    The groups by which to calculate an impute value with a strategy passed to 'impute_with'

    Attributes
    ----------
    is_fitted : boolean
                The indicator that shows whether 'fit' was called

    impute_frame : pandas DataFrame of shape [len(n_group_combinations), n_groups + 1]
    """
    def __init__(self, impute_with, impute_var, impute_groups):
        self.impute_with = impute_with
        self.__init_imputer()
        self.impute_groups = impute_groups
        self.impute_var = impute_var
        self.impute_dict = {}

    def __init_imputer(self):
        self.impute_with = np.mean if self.impute_with == 'mean' else np.median
    
    def fit(self, X):
        self.is_fitted = True
        self.impute_frame = X.groupby(self.impute_groups)[self.impute_var].apply(self.impute_with).reset_index()
        return self

    def transform(self, X, return_series=True):
        index = X.index.copy()
        Y = X.copy()

        Y = pd.merge(left=Y, 
                     right=self.impute_frame, 
                     how='left', 
                     left_on=self.impute_groups, 
                     right_on=self.impute_groups).rename(columns={self.impute_var + '_x': self.impute_var, self.impute_var + '_y': 'imputer'})
        Y.loc[Y[self.impute_var].isna(), self.impute_var] = Y.loc[Y[self.impute_var].isna(), 'imputer'].copy()
        Y.index = index
        return Y[self.impute_var] if return_series else Y.drop(columns='imputer')  
    
    def fit_transform(self, X):
        return self.fit(X).transform(X)

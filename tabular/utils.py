from contextlib import contextmanager
import time
import numpy as np
import pandas as pd
from multiprocessing.pool import Pool, ThreadPool
from itertools import chain, islice, product, accumulate
import subprocess


def head(lst: list) -> 'Any':
    return lst[0]
    
def tail(lst: list) -> 'Any':
    reutrn list[-1]

@contextmanager
def wpbar(*args, **kwargs) -> tqdm:
    pbar = tqdm(*args, **kwargs)
    try:
        yield pbar
    finally:
        pbar.close()


def read_csv_parallel(path, skiprows, nrows, **kwargs):
        return pd.read_csv(path, skiprows=skiprows, nrows=nrows, **kwargs)

class ParallelReader(object):
        def __init__(self, path, n_processes):
                self.path = path
                self.n_processes = n_processes

        def _get_nlines(self):
                return int(subprocess.Popen(f"wc -l {self.path}", shell=True, stdout=subprocess.PIPE).stdout.read().decode('utf-8').split(' ')[0])
        
        def read(self):
                nlines = self._get_nlines()
                nrows, skiprows = self._get_chunks(nlines)
                pool = self._spawn_pool()
                return pd.concat([pool.apply(read_csv_parallel, args=(self.path, y, z)) for y, z in zip(skiprows, nrows)])

        def _get_chunks(self, nlines):
                chunksize = int(nlines/self.n_processes)
                nrows = (chunksize)*self.n_processes
                skiprows = (1) + tuple(accumulate(nrows[1::]))
                return nrows, skiprows
        
        def _spawn_pool(self):
                return multiprocessing.Pool(self.n_processes)
        


@contextmanager
def timer(name: str) -> None:
    t0 = time.time()
    yield
    print(f'[{name}] done in {time.time() - t0:.0f} s')
    

def parallelize(data: pd.DataFrame or np.array, func: callable, partitions: int) -> pd.DataFrame:
    data_split = np.array_split(data.values, partitions)
    pool = Pool(partitions)
    data = pd.concat([pd.Series(l) for l in pool.map(func, data_split)]).values
    pool.close()
    pool.join()
    return data


def parallelize_dataframe(df: pd.DataFrame, func: callable, cores: int = 4) -> pd.DataFrame:
    df_split = np.array_split(df, cores)
    pool = Pool(cores)
    df = pd.concat(pool.map(func, df_split))
    pool.close()
    pool.join()
    return df


def chunks(iterable: 'iterable', size: int =10) -> chain:
    iterator = iter(iterable)
    for first in iterator:
        yield chain([first], islice(iterator, size - 1))

# itertools implementation
def get_all_pairs(users: 'iterable', items: 'iterable') -> product:
        return product(set(users), set(items))

# for-loop implementation - try cython/numba
def get_all_pairs(users: 'iterable', items: 'iterable') -> set:
        add = set()
        for user in set(users):
                for item in set(items):
                        pairs.add(tuple(user, item))
        return pairs

def get_positive_pairs(users: 'iterable', items: 'iterable') -> set:
        return set(zip(users, items))

def get_negative_pairs(pos_pairs: set, all_pairs: set) -> set:
        return set(all_pairs).difference(pos_pairs)

class NegativePairsBuilder(object):
        def __init__(self):
                self.out_names = ('user', 'item')

        def __call__(self, pairs: set) -> pd.DataFrame:
                return pd.DataFrame(list(pairs), columns=self.out_names)

        @classmethod
        def from_frame(cls, df: pd.DataFrame, user_item_cols: 'iterable') -> 'cls':
                users = df[user_item_cols[0]].values
                items = df[user_item_cols[1]].values
                return cls.from_iterables(users, items)

        @classmethod
        def from_iterables(cls, users: 'iterable', items: 'iterable') -> 'cls':
                all_pairs = get_all_pairs(users, items)
                pos_pairs = get_positive_pairs(users, items)
                neg_pairs = get_negative_pairs(pos_pairs, all_pairs)
                return cls()(neg_pairs)
        @classmethod 
        def from_file(cls, path: 'path-like', user_item_index: 'iterable of integers') -> 'cls':
                pass

def get_time_window(series):
        """
                Parameters:
                        series => pandas.Series, dtype = datelike
                Usage:
                        >> dates, slice_getter = get_time_window(df['report_date'])
                        >> print(dates[slice_getter(0, 4)])
                        >> [Timestamp('2017-05-31 00:00:00+0300', tz='pytz.FixedOffset(180)'),
                        >>  Timestamp('2017-06-30 00:00:00+0300', tz='pytz.FixedOffset(180)'),
                        >>  Timestamp('2017-07-31 00:00:00+0300', tz='pytz.FixedOffset(180)'),
                        >> Timestamp('2017-08-31 00:00:00+0300', tz='pytz.FixedOffset(180)')]

        """
    dates = sorted(series.unique())
    length = len(dates)
    def inner(start_from, width, step=1):
            """
                Paramteres:
                        start_from => left time bound, int
                        width => right time bound, int
                        step => window width, int. Default = 1
            """
        slice_ = slice(start_from,
                       start_from + width,
                       step)
        return slice_
    return (dates, inner)
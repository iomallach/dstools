from torch.utils.data import Dataset
import pandas as pd

class EntityEmbedderData(Dataset):
    def __init__(self, df, target, cat_cols, num_cols):
        self.df = df
        self.target_or_index = target
        self.cat_cols = cat_cols
        self.num_cols = num_cols
        
    def __len__(self):
        return self.df.shape[0]
    
    def __getitem__(self, idx):
        X_cat = self.df.iloc[idx][self.cat_cols].values
        X_num = self.df.iloc[idx][self.num_cols].values
        target_or_index = self.target_or_index.iloc[idx] if self.target_or_index else self.df.index.values
        return X_cat, X_num, target_or_index 
    
    @classmethod
    def from_dataframe(cls, df, target, cat_cols, num_cols):
        return cls(df, target, cat_cols, num_cols)
    
    @classmethod 
    def from_path(cls, path, target_col, cat_cols, num_cols, pandas_kw):
        df = pd.read_csv(path) if pandas_kw is None else pd.read_csv(path, **pandas_kw)
        return cls.from_dataframe(df[cat_cols + num_cols], df[target_col], cat_cols, num_cols)

class NeuCFData(Dataset):
    def __init__(self, positives, negatives, user_col, item_col):
        self.positives = positives
        self.negatives = negatives
        self.user_col = user_col
        self.item_col = item_col
    
    def __len__(self):
        return self.positives.shape[0]
    
    def __getitem__(self, idx):
        positive_users = self.positives.iloc[idx][self.user_col].values
        positive_items = self.positives.iloc[idx][self.item_col].values
        # sampling strategy - undefined. Sampler?
        negative_users = self.negatives[self.user_col].sample().values
        negative_items = self.negatives[self.item_col].sample().values
        target_positive = 1
        target_negative = 0
        return (positive_users, positive_items, target_positive, negative_users, negative_items, target_negative)
    
    @classmethod
    def from_dataframes(positives, negatives, user_col, item_col):
        return cls(positives, negatives, user_col, item_col)
    @classmethod
    def from_paths(pos_path, neg_path, user_col, item_col, **kwargs):
        positives = pd.DataFrame(pos_path, **kwargs)
        negatives = pd.DataFrame(neg_path, **kwargs)
        return cls(positives, negatives)

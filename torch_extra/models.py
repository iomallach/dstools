from .utils import get_embs_dims, emb_init
from torch.nn.init import *
import torch
import torch.nn as nn
import torch.nn.functional as F


class EntityEmbeddings(nn.Module):
    def __init__(self, n_classes, emb_szs, n_cont, szs, emb_drops, drops, use_bn):
        super().__init__()
        self.embs = nn.ModuleList([nn.Embedding(m, d) for m, d in emb_szs]) 
        self.use_bn = use_bn
        self.n_classes = n_classes
        # Init embeddings
        for emb in self.embs: emb_init(emb)
        n_emb = sum(e.embedding_dim for e in self.embs) 
        # nn.Linear sizes
        szs = [n_emb+n_cont] + szs
        #  Linear
        self.lins = nn.ModuleList([
            nn.Linear(szs[i], szs[i+1]) for i in range(len(szs)-1)])
        # Batchnorms
        self.bns = nn.ModuleList([
            nn.BatchNorm1d(sz) for sz in szs[1:]])
        # Init linear
        for o in self.lins: kaiming_normal_(o.weight.data)
        # Output layer
        self.outp = nn.Linear(szs[-1], self.n_classes)
        kaiming_normal_(self.outp.weight.data)
        # Dropouts
        self.drops = nn.ModuleList([nn.Dropout(drop) for drop in drops])
        self.emb_drop = nn.Dropout(emb_drops)
        # Continuous features batchnorm
        self.bn = nn.BatchNorm1d(n_cont) 
        
    def forward(self, x_cat, x_cont):
        x = [emb(x_cat[:, i]) for i, emb in enumerate(self.embs)] 
        x = torch.cat(x, 1) 
        x = self.emb_drop(x) 
        x2 = self.bn(x_cont) 
        x = torch.cat([x, x2], 1) 
        for l, d, b in zip(self.lins, self.drops, self.bns):
            x = F.relu(l(x)) 
            if self.use_bn: x = b(x) 
            x = d(x) 
        x = self.outp(x) 
        return F.log_softmax(x, dim=-1) if self.n_classes > 2 else F.sigmoid(x)


class NeuCF(nn.Module):
    def __init__(self, user_emb_conf, item_emb_conf, mlp_conf, mlp_drops=None):
        assert len(mlp_conf) == len(mlp_drops), 'MLP dimensionality mismatch'
        if mlp_drops is None:
            mlp_drops = [x for x in mlp_conf*0]

        self.gmf_embeddins = nn.ModuleList(nn.Embedding(x, y) for (x, y) in zip(user_emb_conf, item_emb_conf))
        self.mlp_embeddings = nn.ModuleList(nn.Embedding(x, y) for (x, y) in zip(user_emb_conf, item_emb_conf))

        self.mlp_lins = nn.ModuleList(
            [nn.Linear(mlp_conf[i], mlp_conf[i+1]) for i in range(len(mlp_conf) - 1)]
                                     )
        self.mlp_drops = nn.ModuleList(
            [nn.Dropout(mlp_drops[i]) for i in mlp_drops]
        )
        self.output = nn.Linear(user_emb_conf[1] + item_emb_conf[1] + mlp_conf[-1], 1)

    
    def forward(self, users, items):
        user_gmf = self.gmf_embeddins[0](users)
        item_gmf = self.gmf_embeddins[1](items)
        gmf_out = user_gmf * item_gmf

        user_mlp = self.mlp_embeddings[0](users)
        item_mlp = self.mlp_embeddings[1](items)
        mlp_out = torch.cat([user_mlp, item_mlp], 1)
        for l, d in zip(self.mlp_lins, self.mlp_drops):
            mlp_out = l(mlp_out)
            mlp_out = F.relu(mlp_out)
            mlp_out = d(mlp_out)
        
        gmf_mlp = torch.cat([gmf_out, mlp_out], 1)
        gmf_mlp = self.output(gmf_mlp)
        
        return F.logsigmoid(gmf_mlp)
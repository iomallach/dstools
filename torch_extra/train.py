import numpy as np
import torch


def validate_minibatch(val_dataloader, model, device, loss_function):
    val_losses = []
    val_preds = []
    val_iter = iter(val_dataloader)
    for X, y in val_iter:
        X, y = X.to(device), y.to(device)
        preds = model(X)
        val_preds.append(preds.cpu().numpy())
        loss = loss_function(preds, y)
        val_losses.append(loss.item())
    return val_preds, torch.mean(val_losses).item()

def validate_batch(val_dataloader, model, device, loss_function):
    val_iter = iter(val_dataloader)
    X_val, y_val = next(val_iter)
    X_val, y_val = X_val.to(device), y_val.to(device)
    for X, y in val_iter:
        X, y = X.to(device), y.to(device)
        X_val, y_val = torch.cat([X_val, X], 0), torch.cat([y_val, y], 0)
    preds = model(X_val)
    loss = loss_function(preds, y_val)
    return preds.cpu().numpy(), loss.item()

def train(loss_function, optimizer, model, train_dataloader, val_dataloader, early_stopping, epochs, device, val_mode='batch', score_func=None):
    per_epoch_loss_train = []
    per_epoch_loss_valid = []
    for epoch in range(epochs):
        train_losses = []
        model.train()
        for X, y in train_dataloader:
            optimizer.zero_grad()
            X, y = X.to(device), y.to(device)
            preds = model(X)
            loss = loss_function(y, preds)
            loss.backward()
            optimizer.step()
            train_losses.append(loss.item())
        train_loss = np.mean(train_losses)

        if val_dataloader:
            model.eval()
            with torch.no_grad():
                if val_mode == 'batch':
                    val_preds, val_loss = validate_batch(val_dataloader, model, device, loss_function)
                else:
                    val_preds, val_loss = validate_minibatch(val_dataloader, model, device, loss_function) 
         
        per_epoch_loss_train.append(train_loss)
        per_epoch_loss_valid.append(val_loss)
        
        print_msg = (f'[{epoch}/{epochs}]' +
                    f'train_loss: {train_loss:.5f} ' +
                    f'valid_loss: {val_loss:.5f}')
    
        early_stopping(val_loss, model)
        if early_stopping.early_stop:
            print('Early stopping')
            break
        
        return model, per_epoch_loss_train, per_epoch_loss_valid


class EarlyStopping:
    """Early stops the training if validation loss doesn't improve after a given patience."""
    def __init__(self, patience=7, verbose=False):
        """
        Args:
            patience (int): How long to wait after last time validation loss improved.
                            Default: 7
            verbose (bool): If True, prints a message for each validation loss improvement. 
                            Default: False
        """
        self.patience = patience
        self.verbose = verbose
        self.counter = 0
        self.best_score = None
        self.early_stop = False
        self.val_loss_min = np.Inf

    def __call__(self, val_loss, model):

        score = -val_loss

        if self.best_score is None:
            self.best_score = score
            self.save_checkpoint(val_loss, model)
        elif score < self.best_score:
            self.counter += 1
            print(f'EarlyStopping counter: {self.counter} out of {self.patience}')
            if self.counter >= self.patience:
                self.early_stop = True
        else:
            self.best_score = score
            self.save_checkpoint(val_loss, model)
            self.counter = 0

    def save_checkpoint(self, val_loss, model):
        '''Saves model when validation loss decrease.'''
        if self.verbose:
            print(f'Validation loss decreased ({self.val_loss_min:.6f} --> {val_loss:.6f}).  Saving model ...')
        torch.save(model.state_dict(), 'checkpoint.pt')
        self.val_loss_min = val_loss
import torch.nn as nn
import torch
import numpy as np


def build_embedding_matrix(vocab, model, dim=300):
    n_rows = len(vocab)
    row = np.zeros(shape=(1, dim))
    embed_matrix = np.empty(shape=(n_rows, dim))
    embed_matrix[0, :] = row
    embed_matrix[1, :] = row

    for (idx, value) in enumerate(vocab):
        if idx < 2:
            continue
        row = model[value]
        embed_matrix[idx, :] = row
    return embed_matrix

def get_emb_layer(embed_matrix):
    voc_size, embed_dim = embed_matrix.shape
    layer = nn.Embedding.from_pretrained(embeddings=torch.from_numpy(embed_matrix).cpu().float(), freeze=True)
    return layer, voc_size, embed_dim

def get_embs_dims(data, cats):
    cat_sz = [data[c].nunique() for c in cats]
    return [(c, min(50, (c+1)//2)) for c in cat_sz]

def emb_init(x):
    x = x.weight.data
    sc = 2/(x.size(1)+1)
    x.uniform_(-sc,sc)